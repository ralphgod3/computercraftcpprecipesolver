#ifndef SIMPLE_FILE_IO_H
#define SIMPLE_FILE_IO_H
#include <string>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <vector>

class SimpleFileIo
{
private:
    SimpleFileIo(){};

public:
    /**
     * @brief check if file exists
     * 
     * @param location filepath to check
     * @return true file exists
     * @return false file does not exist
     */
    static bool fileExists(const std::string& location)
    {
        return std::filesystem::exists(location);
    }

    /**
     * @brief read file from disk and returns contents
     * 
     * @param location file location to read
     * @return std::string file contents
     */
    static std::string readFile(const std::string &location)
    {
        //read file
        std::ifstream fp;
        fp.open(location);
        std::ostringstream sstr;
        sstr << fp.rdbuf();
        fp.close();
        return sstr.str();
    }

    /**
     * @brief Get the contents of a folder
     * 
     * @param path folder to get contents for
     * @return std::vector<std::string> folder contents
     */
    static std::vector<std::string> getFolderContents(const std::string & path)
    {
        std::vector<std::string> files;
        for (const std::filesystem::directory_entry& entry : std::filesystem::directory_iterator(path))
        {
            if(!entry.is_directory())
            {
                files.push_back(entry.path().stem().string());
            }
        }
        return files;
    }

    ~SimpleFileIo(){};
};

#endif