#include <regex>
#include "RecipeDatabase.h"
#include "../SimpleFileIo/SimpleFileIo.h"
#include "../nlohman/json.hpp"

#include <iostream>

#define recipeSetDirectory "DataFiles/MCVERSION/recipeDatabase"
#define recipeSetFileLoc "DataFiles/MCVERSION/recipeDatabase/RECIPESET.json"

RecipeDatabase::RecipeDatabase() {}
RecipeDatabase::~RecipeDatabase() {}

ItemType RecipeDatabase::convertStringToItemType(const std::string& itemString)
{
    if(itemString == "item")
    {
        return ItemType::item;
    }
    else if(itemString == "tag")
    {
        return ItemType::tag;
    }
    return ItemType::unknown;
}

bool RecipeDatabase::recipeTypeIsInFilters(const std::string& recipeType,const std::vector<std::string>& recipeTypeFilters)
{
    if(recipeTypeFilters.size() == 0)
    {
        return true;
    }
    for(const std::string& recipe : recipeTypeFilters)
    {
        if(recipeType == recipe)
        {
            return true;
        }
    }
    return false;
}

bool RecipeDatabase::itemTypeIsInFilters(const ItemType& itemType, const std::vector<ItemType>& itemFilters)
{
    if(itemFilters.size() == 0)
    {
        return true;
    }
    for(const ItemType& item : itemFilters)
    {
        if(itemType == item)
        {
            return true;
        }
    }
    return false;
}

bool RecipeDatabase::init(const std::string &mcVersion, const std::string &recipeSet)
{
    std::string fileLoc = recipeSetFileLoc;
    fileLoc = std::regex_replace(fileLoc, std::regex("MCVERSION"), mcVersion);
    fileLoc = std::regex_replace(fileLoc, std::regex("RECIPESET"), recipeSet);
    if(! SimpleFileIo::fileExists(fileLoc))
    {
        return false;
    }

    std::string contents = SimpleFileIo::readFile(fileLoc);
    p_recipeDatabase.clear();
    nlohmann::json fileDbJson = nlohmann::json::parse(contents);
    for (auto& el : fileDbJson.items())
    {
        RecipeInfo* recipeInfo = new RecipeInfo();
        recipeInfo->name = el.value()["name"];
        recipeInfo->type = convertStringToItemType(el.value()["type"]);
        recipeInfo->nbt = "";
        if(el.value()["nbt"] != nlohmann::detail::value_t::null)
        {
            recipeInfo->nbt = el.value()["nbt"];
        }
        recipeInfo->recipes.clear();
        //get recipes
        for (auto& elItem : el.value()["recipes"].items())
        {
            Recipe* recipe = new Recipe();
            recipe->type = elItem.value()["type"];
            recipe->count = elItem.value()["count"];
            //optional outputs
            if(elItem.value()["optional"] != nlohmann::detail::value_t::null)
            {
                for (auto& optOutput : elItem.value()["optional"].items())
                {
                    ItemEntry itemEntry = ItemEntry();
                    itemEntry.count = optOutput.value()["count"];
                    itemEntry.name = optOutput.value()["name"];
                    itemEntry.type = convertStringToItemType(optOutput.value()["type"]);
                    if(optOutput.value()["nbt"] != nlohmann::detail::value_t::null)
                    {
                        itemEntry.nbt = optOutput.value()["nbt"];
                    }
                    recipe->optionalItems.push_back(itemEntry);
                }
            }
            //deal with recipe itself
            for (auto& recipeItem : elItem.value()["recipe"].items())
            {
                ItemEntry itemEntry = ItemEntry();
                itemEntry.count = recipeItem.value()["count"];
                itemEntry.name = recipeItem.value()["name"];
                itemEntry.type = convertStringToItemType(recipeItem.value()["type"]);
                if(recipeItem.value()["nbt"] != nlohmann::detail::value_t::null)
                {
                    itemEntry.nbt = recipeItem.value()["nbt"];
                }
                recipe->recipe[stoi(recipeItem.key())] = itemEntry;
            }
            recipeInfo->recipes.push_back(recipe);
        }
        p_recipeDatabase.push_back(recipeInfo);
    }

    return true;
}

std::vector<RecipeInfo> RecipeDatabase::getAllRecipesForType(const std::string &type)
{
    std::vector<RecipeInfo> retVector = std::vector<RecipeInfo>(); 
    for( RecipeInfo* recipeInfo : p_recipeDatabase)
    {
        RecipeInfo rInfo = RecipeInfo();
        rInfo.name = recipeInfo->name;
        rInfo.nbt = recipeInfo->nbt;
        rInfo.type = recipeInfo->type;
        bool add = false;
        for(std::size_t i = 0; i < recipeInfo->recipes.size(); i++)
        {
            if (recipeInfo->recipes[i]->type == type)
            {
                add = true;
                rInfo.recipes.push_back(recipeInfo->recipes[i]);
            }
        }
        if(add)
        {
            retVector.push_back(rInfo);
        }
    }
    return retVector;
}

std::vector<std::string> RecipeDatabase::getRecipeTypes()
{
    std::vector<std::string> retVector;
    for(RecipeInfo* recipeInfo : p_recipeDatabase)
    {
        for(Recipe* recipe : recipeInfo->recipes)
        {
            bool exists = false;
            for(std::string& str : retVector)
            {
                if(str == recipe->type)
                {
                    exists = true;
                    break;
                }
            }
            if(!exists)
            {
                retVector.push_back(recipe->type);
            }
        }
    }
    return retVector;
}




std::vector<Recipe *> RecipeDatabase::getRecipesFor(const std::string &name, const std::string &nbt, const std::vector<std::string> recipeTypeFilters, const std::vector<ItemType> itemTypeFilters)
{
    
    for(RecipeInfo* recipeInfo : p_recipeDatabase)
    {
        if(recipeInfo->name == name && recipeInfo->nbt == nbt && itemTypeIsInFilters(recipeInfo->type, itemTypeFilters))
        {
            std::vector<Recipe *> retRecipes;
            for(Recipe* recipe : recipeInfo->recipes)
            {
                bool addRecipe = false;
                if(recipeTypeIsInFilters(recipe->type, recipeTypeFilters))
                {
                    if(itemTypeFilters.size() == 0)
                    {
                        addRecipe = true;
                    }
                    else
                    {
                        bool containsFilteredItemType = false;
                        for(const auto& entry : recipe->recipe)
                        {
                            if(!itemTypeIsInFilters(entry.second.type, itemTypeFilters))
                            {
                                containsFilteredItemType = true;
                                break;
                            }
                        }
                        if(! containsFilteredItemType)
                        {
                            addRecipe = true;
                        }
                    }
                }
                if(addRecipe)
                {
                    retRecipes.push_back(recipe);
                }
            }
            return retRecipes;
        }
    }
    return std::vector<Recipe*>();
}

std::vector<std::string> RecipeDatabase::getRecipeSetOptions(const std::string &mcVersion)
{
    std::vector<std::string> ret;
    std::string fileLoc = recipeSetDirectory;
    fileLoc = std::regex_replace(fileLoc, std::regex("MCVERSION"), mcVersion);
    try
    {
        ret = SimpleFileIo::getFolderContents(fileLoc);
    }
    catch(const std::exception& e){}
    return ret;
}