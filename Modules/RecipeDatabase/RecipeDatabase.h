#ifndef RECIPE_DATABASE_H
#define RECIPE_DATABASE_H

#include <vector>
#include <string>
#include <unordered_map>


enum class ItemType
{
    item,
    tag,
    unknown,
};
struct ItemEntry
{
    ItemType type;
    std::string name;
    int count;
    std::string displayName;
    std::string nbt;
    ItemEntry(){}
    ItemEntry(const std::string& name, const std::string& nbt,int count)
    {
        this->name = name;
        this->nbt = nbt;
        this->count = count;
    }
};
struct Recipe
{
    std::string type;
    int count;
    std::vector<ItemEntry> optionalItems;
    std::unordered_map<int, ItemEntry> recipe;
};
struct RecipeInfo
{
    ItemType type;
    std::string name;
    std::string nbt;
    std::vector<Recipe*> recipes;
};

static std::string toString(ItemType* item)
{
    if(*item == ItemType::item)
    {
        return "item";
    }
    else if(*item == ItemType::tag)
    {
        return "tag";
    }
    return "unknown";
}

static std::string toString(ItemEntry* entry)
{
    return "name " + entry->name + ", type " + toString(&entry->type) + ", count " + std::to_string(entry->count) + ", nbt " + entry->nbt + "\r\n";
}

static std::string toString(Recipe* recipe)
{
    std::string recipeString = "";
    for(auto& entry : recipe->recipe)
    {
        recipeString = recipeString + " slot " + std::to_string(entry.first) + " " + toString(&entry.second);
    }
    return "type " + recipe->type + ", count " + std::to_string(recipe->count) + "\r\n" + recipeString + "\r\n";
}

class RecipeDatabase
{
private:
    std::vector<RecipeInfo*> p_recipeDatabase = std::vector<RecipeInfo*>();

    /**
     * @brief checks if recipeType is in the filters also returns true if filters are empty
     * 
     * @param recipeType type to check
     * @param recipeTypeFilters filters to check against
     * @return true recipetype in filters
     * @return false recipetype not in filters
     */
    static bool recipeTypeIsInFilters(const std::string& recipeType,const std::vector<std::string>& recipeTypeFilters);

    /**
     * @brief checks if itemType is in the filters also returns true if filters are empty
     * 
     * @param itemType type to check
     * @param itemFilters filters to check against
     * @return true itemType is in filters
     * @return false itemType not in filters
     */
    static bool itemTypeIsInFilters(const ItemType& itemType, const std::vector<ItemType>& itemFilters);
public:
    /**
     * @brief Construct a new Recipe Database object
     * 
     */
    RecipeDatabase();
    /**
     * @brief Destroy the Recipe Database object
     * 
     */
    ~RecipeDatabase();

    /**
     * @brief convert itemType string to enum
     * 
     * @param itemString string to convert
     * @return itemType type
     */
    static ItemType convertStringToItemType(const std::string& itemString);

    /**
     * @brief initialize recipe database
     * 
     * @param mcVersion minecraft version ex: 1.16.5
     * @param recipeSet name of the recipe set ex: dw20Modified
     * @return true initialization sucesfull
     * @return false initialization failed
     */
    bool init(const std::string& mcVersion, const std::string& recipeSet);

    /**
     * @brief Get the All Recipes For Type of operation
     * 
     * @param type operation type ex: minecraft:smelting
     * @return std::vector<RecipeInfo> vector that contains all entries for operation type
     */
    std::vector<RecipeInfo> getAllRecipesForType(const std::string& type);

    /**
     * @brief Get the Recipe Types available in recipe set
     * 
     * @return std::vector<std::string> recipe types that are available in recipe set
     */
    std::vector<std::string> getRecipeTypes();

    /**
     * @brief Get the Recipes For item with nbt
     * 
     * @param name itemName or itemId
     * @param nbt nbt of item or "" if no nbt
     * @param recipeTypeFilters limits results to this recipe type, empty vector for no filters 
     * @param itemTypeFilters limits results to these item types empty vector for no filters
     * @return std::vector<Recipe*> list of recipes for item
     */
    std::vector<Recipe*> getRecipesFor(const std::string& name, const std::string& nbt, const std::vector<std::string> recipeTypeFilters, const std::vector<ItemType> itemTypeFilters);
    
    /**
     * @brief Get the Recipe Set Options available for mc version
     * 
     * @param mcVersion mc version to get options for
     * @return std::vector<std::string> list of options
     */
    static std::vector<std::string> getRecipeSetOptions(const std::string & mcVersion);
};


#endif