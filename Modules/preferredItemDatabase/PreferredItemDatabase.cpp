#include <iostream>
#include <regex>
#include "PreferredItemDatabase.h"
#include "../nlohman/json.hpp"

PreferredItemDatabase::PreferredItemDatabase() {}

PreferredItemDatabase::~PreferredItemDatabase() {}

bool PreferredItemDatabase::init(const std::string &preferredItemJson)
{
    if(preferredItemJson == "")
    {
        return true;
    }
    //read tagDb
    p_tagDatabase.clear();
    try
    {
        nlohmann::json fileDbJson = nlohmann::json::parse(preferredItemJson);
        for (auto& el : fileDbJson.items())
        {
            std::vector<std::string> tagContents;
            for (auto& elItem : el.value().items())
            {
                tagContents.push_back(elItem.value());
            }
            p_tagDatabase[el.key()] = tagContents;
        }
    }
    catch(const std::exception& e)
    {
        return false;
    }
    return true;
}

std::vector<std::string> PreferredItemDatabase::getItemsWithTag(const std::string &tag)
{
    if(p_tagDatabase.find(tag) == p_tagDatabase.end())
    {
        return std::vector<std::string>();
    }

    return p_tagDatabase[tag];
}