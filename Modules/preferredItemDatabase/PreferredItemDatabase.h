#ifndef PREFERRED_ITEM_DATABASE_H
#define PREFERRED_ITEM_DATABASE_H

#include <string>
#include <vector>
#include <array>
#include <unordered_map>


class PreferredItemDatabase
{
private:
    std::unordered_map<std::string, std::vector<std::string>> p_tagDatabase = std::unordered_map<std::string, std::vector<std::string>>();
    
public:

    /**
     * @brief Construct a new Item Database object
     * 
     */
    PreferredItemDatabase();

    /**
     * @brief Destroy the Item Database object
     * 
     */
    ~PreferredItemDatabase();

    /**
     * @brief initialize item database
     * 
     * @param mcVersion minecraft version to load
     * @return true init succesful
     * @return false init failed
     */
    bool init(const std::string& preferredItemJson);

    /**
     * @brief Get items that have specified tag
     * 
     * @param tag tag to get items for
     * @return std::vector<std::string> items with tag, empty vector if no item has tag 
     */
    std::vector<std::string> getItemsWithTag(const std::string& tag);
};


#endif