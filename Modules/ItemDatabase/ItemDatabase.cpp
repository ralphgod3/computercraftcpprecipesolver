#include <iostream>
#include <regex>
#include "ItemDatabase.h"
#include "../nlohman/json.hpp"
#include "../SimpleFileIo/SimpleFileIo.h"


#define tagFile "DataFiles/MCVERSION/itemDatabase/Tags.json"
#define itemFile "DataFiles/MCVERSION/itemDatabase/Items.json"

ItemDatabase::ItemDatabase() {}

ItemDatabase::~ItemDatabase() {}

bool ItemDatabase::init(const std::string &mcVersion)
{
    //read itemDb
    std::string itemFileLoc = itemFile;
    std::string tagFileLoc = tagFile;
    itemFileLoc = std::regex_replace(itemFileLoc, std::regex("MCVERSION"), mcVersion);
    tagFileLoc = std::regex_replace(tagFileLoc, std::regex("MCVERSION"), mcVersion);
    if(! SimpleFileIo::fileExists(itemFileLoc) || ! SimpleFileIo::fileExists(tagFileLoc))
    {
        return false;
    }
    std::string contents = SimpleFileIo::readFile(itemFileLoc);
    p_itemDatabase.clear();
    nlohmann::json fileDbJson = nlohmann::json::parse(contents);
    for (auto& el : fileDbJson.items())
    {
        std::unordered_map<std::string, Item*> itemMap;
        for (auto& elItem : el.value().items())
        {
            Item* i = new Item();
            i->displayName = elItem.value()["displayName"];
            i->maxCount = elItem.value()["maxCount"];
            if( nlohmann::detail::value_t::null != elItem.value()["maxDamage"])
            {
                i->maxDamage = elItem.value()["maxDamage"];
            }
            itemMap[elItem.key()] = i;
        }
        p_itemDatabase[el.key()] = itemMap;
    }
    //read tagDb
    contents = SimpleFileIo::readFile(tagFileLoc);
    p_tagDatabase.clear();
    fileDbJson = nlohmann::json::parse(contents);
    for (auto& el : fileDbJson.items())
    {
        std::vector<std::string> tagContents;
        for (auto& elItem : el.value().items())
        {
            tagContents.push_back(elItem.value());
        }
        p_tagDatabase[el.key()] = tagContents;
    }
    return true;
}

std::vector<std::string> ItemDatabase::getItemsWithTag(const std::string &tag)
{
    if(p_tagDatabase.find(tag) == p_tagDatabase.end())
    {
        return std::vector<std::string>();
    }

    return p_tagDatabase[tag];
}

Item* ItemDatabase::getItem(const std::string &itemName, const std::string &nbt)
{
    std::string nbtInternal = "None";
    if(nbt != "None" && nbt != "")
    {
        nbtInternal = nbt;
    }
    if(! this->itemIsInDb(itemName, nbtInternal))
    {
        return nullptr;
    }

    return p_itemDatabase[itemName][nbtInternal];
}

bool ItemDatabase::itemIsInDb(const std::string &itemName, const std::string &nbt)
{
    if(p_itemDatabase.find(itemName) == p_itemDatabase.end())
    {
        return false;
    }
    std::string nbtInternal = "None";
    if(nbt != "None" && nbt != "")
    {
        nbtInternal = nbt;
    }
    if(p_itemDatabase[itemName].find(nbtInternal) == p_itemDatabase[itemName].end())
    {
        return false;
    }
    return true;
}