#ifndef ITEM_DATABASE_H
#define ITEM_DATABASE_H

#include <string>
#include <vector>
#include <array>
#include <unordered_map>
struct Item
{
    std::vector<std::string> tags = std::vector<std::string>();
    uint16_t maxCount = 0;
    std::string displayName = "";
    int maxDamage = 0;
};


class ItemDatabase
{
private:
    std::unordered_map<std::string, std::unordered_map<std::string,Item*>> p_itemDatabase = std::unordered_map<std::string, std::unordered_map<std::string, Item*>>();
    std::unordered_map<std::string, std::vector<std::string>> p_tagDatabase = std::unordered_map<std::string, std::vector<std::string>>();
    
public:

    /**
     * @brief Construct a new Item Database object
     * 
     */
    ItemDatabase();

    /**
     * @brief Destroy the Item Database object
     * 
     */
    ~ItemDatabase();

    /**
     * @brief initialize item database
     * 
     * @param mcVersion minecraft version to load
     * @return true init succesful
     * @return false init failed
     */
    bool init(const std::string& mcVersion);

    /**
     * @brief Get items that have specified tag
     * 
     * @param tag tag to get items for
     * @return std::vector<std::string> items with tag, empty vector if no item has tag 
     */
    std::vector<std::string> getItemsWithTag(const std::string& tag);
    /**
     * @brief Get the Item with itemName and nbt
     * 
     * @param itemName itemId of the item to get
     * @param nbt optional nbt enter empty string for no nbt
     * @return Item* item if found, nullpointer if not found 
     */
    Item* getItem(const std::string& itemName, const std::string& nbt);

    /**
     * @brief returns true if item is in database
     * 
     * @param itemName name of item to check for
     * @param nbt nbt of item to check for, "" if no nbt
     * @return true item is in db
     * @return false item is not in db
     */
    bool itemIsInDb(const std::string& itemName, const std::string& nbt);
};


#endif