#ifndef RECIPE_SOLVER_H
#define RECIPE_SOLVER_H

#include <list>
#include <math.h>
#include "../RecipeDatabase/RecipeDatabase.h"
#include "../ItemDatabase/ItemDatabase.h"
#include "../preferredItemDatabase/PreferredItemDatabase.h"


struct CraftingSolution
{
    std::string message;
};




class RecipeSolver
{
private:
    /**
     * @brief replaces any occurance of tagToReplace with itemToReplaceTagWith in recipe and returns new recipe
     * 
     * @param recipe recipe to replace tags in
     * @param tagToReplace tag to replace with item
     * @param itemToReplaceTagWith item to replace tag with
     * @return Recipe new recipe
     */
    static Recipe replaceTagInRecipe(const Recipe* recipe, const std::string& tagToReplace, const std::string& itemToReplaceTagWith);
    /**
     * @brief increments curTagReplacement to next item variant
     * 
     * @param curTagReplacement current tag replacements
     * @param tagReplacements replacements for tags
     * @return true tag is incremented
     * @return false no more increments left
     */
    static bool incrementItemTagReplacement(std::vector<std::string>* curTagReplacement, const std::vector<std::vector<std::string>>& tagReplacements);

    /**
     * @brief Get all variants for a recipe by replacing any tags with items
     * 
     * @param recipe recipe to get variants for
     * @param itemDb item database
     * @param preferredItemDb preferred item database
     * @return std::vector<Recipe> recipe variants
     */
    static std::vector<Recipe> getRecipeVariants(const Recipe* recipe, ItemDatabase& itemDb, PreferredItemDatabase& preferredItemDb);

    /**
     * @brief get count of items needed to craft recipe
     * 
     * @param recipe recipe to get costs for
     * @param timesCrafted amount of times recipe will be crafted
     * @return std::vector<ItemEntry> recipe costs
     */
    static std::vector<ItemEntry> getRecipeCost(Recipe* recipe, unsigned int timesCrafted);

    /**
     * @brief gets items in itemsToGet table from storage or adds them to a list for items to be crafted later
     * 
     * @param itemsToGet items that should be gotten (requested or crafted)
     * @param itemsInStorage items that are in storage system
     * @return std::array<std::vector<ItemEntry>,3> itemsInStorage items in storage after subtracting itemsToRequest, itemsToRequest items that can be requested, itemsToCraft items that have to be crafted
     */
    static std::array<std::vector<ItemEntry>,3> getItems(const std::vector<ItemEntry>& itemsToGet,const std::vector<ItemEntry>& itemsInStorage);

    /**
     * @brief Get item from storage if possible
     * 
     * @param itemToGet item to get from storage
     * @param itemsInStorage items in storage (will be modified)
     * @return std::pair<ItemEntry, ItemEntry> itemsToCraft items that could not be gotten from storage system and have to be crafted, itemsGotten items that could be gotten from storage system (returns count 0 if no items could be gotten)
     */
    static std::pair<ItemEntry, ItemEntry> getItemFromStorage(ItemEntry itemToGet, std::vector<ItemEntry>& itemsInStorage);

    /**
     * @brief describes an entry in the crafting chain
     * 
     */
    struct CraftingChainEntry
    {
        Recipe recipe;
        std::string creates;
        std::vector<std::string> requires;
        unsigned int craftsNeeded;
        unsigned int outputPerCraft;
        unsigned int count = 0;
        unsigned int itemsCreated;
        /**
         * @brief Construct a new Crafting Chain Entry
         * 
         * @param recipe recipe for entry
         * @param requires required recipes to be completed before this oen can be started
         * @param count items required as output from recipe NOT CRAFTS REQUIRED so if recipe produces 4 and you need 8 you put in 8 not 2
         * @param itemProduced itemId produced as a result from recipe
         */
        CraftingChainEntry(Recipe& recipe, std::vector<std::string>& requires, unsigned int count,const std::string& itemProduced)
        {
            this->recipe = recipe;
            this->creates = itemProduced;
            this->requires = requires;
            this->craftsNeeded = ceil(count/ recipe.count);
            this->outputPerCraft = recipe.count;
            this->count = 0;
            this->itemsCreated = craftsNeeded * outputPerCraft;
        }
        
    };

    struct BranchInfo
    {
        std::vector<RecipeSolver::CraftingChainEntry> craftingChain;
        std::vector<ItemEntry> itemsInStorage;
        std::vector<ItemEntry> itemsToRequest;
        std::vector<ItemEntry> itemsToCraft;
        std::vector<Recipe*> recipesUsed;
    };

    /**
     * @brief check if branch is solved
     * 
     * @param branchInfo 
     * @return true branch is solved
     * @return false branch is not solved
     */
    static bool branchIsSolved(BranchInfo& branchInfo);
    /**
     * @brief merges 2 itemEntry vectors into 1
     * 
     * @param vector1 vector 1 to merge
     * @param vector2 vector 2 to merge
     * @return std::vector<ItemEntry> merged vector
     */
    static std::vector<ItemEntry> mergeItemEntryVectors(const std::vector<ItemEntry>& vector1, const std::vector<ItemEntry>& vector2);

    /**
     * @brief Get the Best Crafting Solution
     * 
     * @param solvedBranches branches that are solved
     * @return BranchInfo best crafting solution
     */
    static BranchInfo getBestCraftingSolution(const std::vector<BranchInfo>& solvedBranches);

    static bool itemsToCraftVectorsAreEqual(const std::vector<ItemEntry>& vec1, const std::vector<ItemEntry>& vec2);

    /**
     * @brief generate list of craftable recipes
     * 
     */
    void updateCraftableRecipeCache();
    std::vector<ItemEntry> craftableRecipes;

    /**
     * @brief Create a Initial Branches for recipe solver
     * 
     * @param itemId item id
     * @param count amount of items to craft
     * @param nbt nbt value of item to produce or "" for no nbt
     * @param itemsInStorage items currently in storage
     * @return std::list<BranchInfo> initial branches for solver
     */
    std::list<BranchInfo> createInitialBranches(const std::string& itemId, unsigned int count, const std::string& nbt, const std::vector<ItemEntry>& itemsInStorage);


    ItemDatabase* p_itemDb;
    RecipeDatabase* p_recipeDatabase;
    std::vector<std::string> p_recipeTypesToUse;
    PreferredItemDatabase * p_preferredItemDatabase;
    std::vector<ItemEntry> p_craftableItemCache;
public:
    RecipeSolver();
    ~RecipeSolver();
    /**
     * @brief 
     * 
     * @param itemDb 
     * @param recipeDatabase 
     * @param recipeTypesToUse 
     * @param preferredItemDatabase 
     * @return true 
     * @return false 
     */
    bool init(ItemDatabase& itemDb, RecipeDatabase& recipeDatabase, std::vector<std::string> recipeTypesToUse, PreferredItemDatabase& preferredItemDatabase);

    /**
     * @brief solve a crafting recipe
     * 
     * @param itemId itemName to craft
     * @param count number of items to craft
     * @param nbt nbt data of item to craft or "" for no nbt
     * @param itemsInStorage items currently in storage
     * @param timeout timeout when to kill solver 0 = infinite
     * @return std::pair<bool, CraftingSolution> boolean for sucess, on sucess crafting solution on failure sets message field of crafting solution 
     */
    std::pair<bool, CraftingSolution> solve(const std::string& itemId, unsigned int count, const std::string& nbt, const std::vector<ItemEntry>& itemsInStorage, unsigned int timeout = 0);

    /**
     * @brief dumps a list of all craftable items
     * 
     * @return std::vector<ItemEntry> dumps list of craftable items
     */
    std::vector<ItemEntry> dumpCraftableItems();
};

#endif