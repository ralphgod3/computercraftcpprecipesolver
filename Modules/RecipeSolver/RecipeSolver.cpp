#include <chrono>
#include <iostream>
#include <list>
#include "RecipeSolver.h"

std::vector<ItemType> itemTypeFilters = {ItemType::item, ItemType::tag};
size_t maxBranchDepth = 100;

Recipe RecipeSolver::replaceTagInRecipe(const Recipe *recipe, const std::string &tagToReplace, const std::string &itemToReplaceTagWith)
{
    Recipe outRecipe = Recipe(*recipe);
    for (auto &info : outRecipe.recipe)
    {
        if (info.second.name == tagToReplace)
        {
            info.second.type = ItemType::item;
            info.second.name = itemToReplaceTagWith;
        }
    }
    return outRecipe;
}

bool RecipeSolver::incrementItemTagReplacement(std::vector<std::string> *curTagReplacement, const std::vector<std::vector<std::string>> &tagReplacements)
{
    std::vector<std::string> &curTagRef = *curTagReplacement;
    unsigned int curTagIndex = 0;
    while (true)
    {
        if (curTagRef[curTagIndex] == tagReplacements[curTagIndex][tagReplacements[curTagIndex].size() - 1])
        {
            curTagRef[curTagIndex] = tagReplacements[curTagIndex][0];
            curTagIndex += 1;
            if (curTagIndex > tagReplacements.size() - 1)
            {
                return false;
            }
        }
        else
        {
            for (size_t i = 0; i < tagReplacements[curTagIndex].size(); i++)
            {
                if (curTagRef[curTagIndex] == tagReplacements[curTagIndex][i])
                {
                    curTagRef[curTagIndex] = tagReplacements[curTagIndex][i + 1];
                    return true;
                }
            }
        }
    }
}

std::vector<Recipe> RecipeSolver::getRecipeVariants(const Recipe *recipe, ItemDatabase &itemDb, PreferredItemDatabase &preferredItemDb)
{
    std::vector<std::vector<std::string>> tagItems;
    std::vector<std::string> tagReplacements;
    unsigned int replacedCount = 0;
    for (auto &el : recipe->recipe)
    {
        if (el.second.type == ItemType::tag)
        {
            bool exists = false;
            if (replacedCount != 0)
            {
                for (size_t i = 0; i < tagReplacements.size(); i++)
                {
                    if (tagReplacements[i] == el.second.name)
                    {
                        exists = true;
                        break;
                    }
                }
            }
            if (!exists)
            {
                std::vector<std::string> items;
                items = preferredItemDb.getItemsWithTag(el.second.name);
                if (items.size() == 0)
                {
                    items = itemDb.getItemsWithTag(el.second.name);
                }
                tagReplacements.push_back(el.second.name);
                tagItems.push_back(items);

                replacedCount++;
            }
        }
    }
    std::vector<Recipe> recipeVariants;
    if (tagReplacements.size() > 0)
    {
        //create structure and set all tags to first variant
        std::vector<std::string> tagsReplacedWithItems;
        for (size_t i = 0; i < tagItems.size(); i++)
        {
            tagsReplacedWithItems.push_back(tagItems[i][0]);
        }

        bool tagsReplaced = true;
        while (tagsReplaced)
        {
            Recipe curRecipe = Recipe(*recipe);
            for (size_t i = 0; i < tagsReplacedWithItems.size(); i++)
            {
                curRecipe = replaceTagInRecipe(&curRecipe, tagReplacements[i], tagsReplacedWithItems[i]);
            }
            recipeVariants.push_back(curRecipe);
            tagsReplaced = incrementItemTagReplacement(&tagsReplacedWithItems, tagItems);
        }
    }
    if (recipeVariants.size() == 0)
    {
        recipeVariants.push_back(Recipe(*recipe));
    }
    return recipeVariants;
}

std::vector<ItemEntry> RecipeSolver::getRecipeCost(Recipe *recipe, unsigned int timesCrafted)
{
    std::vector<ItemEntry> costs;
    for (auto &el : recipe->recipe)
    {
        bool exists = false;
        for (size_t i = 0; i < costs.size(); i++)
        {
            if (el.second.name == costs[i].name && el.second.nbt == costs[i].nbt)
            {
                exists = true;
                costs[i].count += (el.second.count * timesCrafted);
                break;
            }
        }
        if (!exists)
        {
            ItemEntry item;
            item.name = el.second.name;
            item.nbt = el.second.nbt;
            item.count = (el.second.count * timesCrafted);
            item.type = el.second.type;
            item.displayName = el.second.displayName;
            costs.push_back(item);
        }
    }
    return costs;
}

std::array<std::vector<ItemEntry>, 3> RecipeSolver::getItems(const std::vector<ItemEntry> &itemsToGet, const std::vector<ItemEntry> &itemsInStorage)
{
    std::vector<ItemEntry> copyOfItemsInStorage = std::vector<ItemEntry>(itemsInStorage);
    std::vector<ItemEntry> itemsToCraft;
    std::vector<ItemEntry> itemsToRequest;

    for (auto &el : itemsToGet)
    {
        std::pair<ItemEntry, ItemEntry> result = getItemFromStorage(el, copyOfItemsInStorage);
        if (result.first.count > 0)
        {
            itemsToCraft.push_back(result.first);
        }
        if (result.second.count > 0)
        {
            itemsToRequest.push_back(result.second);
        }
    }
    return std::array<std::vector<ItemEntry>, 3>() = {copyOfItemsInStorage, itemsToRequest, itemsToCraft};
}

std::pair<ItemEntry, ItemEntry> RecipeSolver::getItemFromStorage(ItemEntry itemToGet, std::vector<ItemEntry> &itemsInStorage)
{
    ItemEntry itemsGotten = ItemEntry(itemToGet);
    itemsGotten.count = 0;

    for (size_t i = 0; i < itemsInStorage.size(); i++)
    {
        ItemEntry &el = itemsInStorage[i];
        if (itemsInStorage[i].name == itemToGet.name && itemsInStorage[i].nbt == itemToGet.nbt)
        {
            int fromStorage = std::min<int>(itemToGet.count, itemsInStorage[i].count);
            el.count -= fromStorage;
            itemToGet.count -= fromStorage;
            if (itemsInStorage[i].count == 0)
            {
                itemsInStorage.erase(itemsInStorage.begin() + i);
            }

            itemsGotten.count = fromStorage;
            break;
        }
    }
    return std::pair<ItemEntry, ItemEntry>(itemToGet, itemsGotten);
}

bool RecipeSolver::branchIsSolved(BranchInfo &branchInfo)
{
    if (branchInfo.itemsToCraft.size() == 0)
    {
        return true;
    }
    return false;
}

std::vector<ItemEntry> RecipeSolver::mergeItemEntryVectors(const std::vector<ItemEntry> &vector1, const std::vector<ItemEntry> &vector2)
{
    std::vector<ItemEntry> outVec = std::vector<ItemEntry>(vector1);
    for (size_t i2 = 0; i2 < vector2.size(); i2++)
    {
        bool exists = false;
        for (size_t i = 0; i < outVec.size(); i++)
        {
            if (outVec[i].name == vector2[i2].name && outVec[i].nbt == vector2[i2].nbt)
            {
                outVec[i].count += vector2[i2].count;
                exists = true;
                break;
            }
        }
        if (!exists)
        {
            outVec.push_back(vector2[i2]);
        }
    }
    return outVec;
}

RecipeSolver::BranchInfo RecipeSolver::getBestCraftingSolution(const std::vector<BranchInfo> &solvedBranches)
{
    if (solvedBranches.size() == 0)
    {
        return BranchInfo();
    }
    BranchInfo branch = BranchInfo(solvedBranches[0]);
    for (auto &el : solvedBranches)
    {
        if (el.craftingChain.size() < branch.craftingChain.size())
        {
            branch = BranchInfo(el);
        }
    }
    return branch;
}

bool RecipeSolver::itemsToCraftVectorsAreEqual(const std::vector<ItemEntry>& vec1, const std::vector<ItemEntry>& vec2)
{
    if(vec1.size() != vec2.size())
    {
        return false;
    }
    for(size_t i = 0; i< vec1.size(); i++)
    {
        bool exists = false;
        for(size_t j = 0; j<vec2.size(); j++)
        {
            if(vec1[i].name == vec2[j].name)
            {
                exists = true;
                break;
            }
        }
        if(!exists)
        {
            return false;
        }
    }
    return true;
}

void RecipeSolver::updateCraftableRecipeCache()
{
    p_craftableItemCache.clear();
    for (size_t i = 0; i < p_recipeTypesToUse.size(); i++)
    {
        std::vector<RecipeInfo> recipes = p_recipeDatabase->getAllRecipesForType((p_recipeTypesToUse)[i]);
        for (size_t j = 0; j < recipes.size(); j++)
        {
            if (recipes[j].type == ItemType::item)
            {
                bool exists = false;
                for (auto &el : p_craftableItemCache)
                {
                    if (el.name == recipes[j].name && el.nbt == recipes[j].nbt)
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                {
                    Item *info = p_itemDb->getItem(recipes[j].name, recipes[j].nbt);
                    if (info != nullptr)
                    {
                        ItemEntry entry;
                        entry.name = recipes[j].name;
                        entry.nbt = recipes[j].nbt;
                        entry.displayName = info->displayName;
                        p_craftableItemCache.push_back(entry);
                    }
                }
            }
        }
    }
}

std::list<RecipeSolver::BranchInfo> RecipeSolver::createInitialBranches(const std::string &itemId, unsigned int count, const std::string &nbt, const std::vector<ItemEntry> &itemsInStorage)
{
    std::list<RecipeSolver::BranchInfo> branchInfo;
    std::vector<Recipe *> recipes = p_recipeDatabase->getRecipesFor(itemId, nbt, p_recipeTypesToUse, itemTypeFilters);
    if (recipes.size() == 0)
    {
        return branchInfo;
    }
    for (size_t i = 0; i < recipes.size(); i++)
    {
        std::vector<Recipe> variants = getRecipeVariants(recipes[i], *p_itemDb, *p_preferredItemDatabase);
        for (size_t j = 0; j < variants.size(); j++)
        {
            RecipeSolver::BranchInfo branchEntry;
            branchEntry.recipesUsed.push_back(recipes[i]);
            unsigned int craftsNeeded = ceil<int>(count / variants[j].count);
            std::vector<ItemEntry> itemCosts = getRecipeCost(&variants[j], craftsNeeded);
            std::array<std::vector<ItemEntry>, 3> getItemsRet = getItems(itemCosts, itemsInStorage);

            std::vector<std::string>
            requires;
            for (size_t k = 0; k < getItemsRet[2].size(); k++)
            {
                requires.push_back(getItemsRet[2][k].name);
            }
            branchEntry.craftingChain.push_back(RecipeSolver::CraftingChainEntry(variants[j], requires, count, itemId));
            branchEntry.itemsInStorage = getItemsRet[0];
            branchEntry.itemsToRequest = getItemsRet[1];
            branchEntry.itemsToCraft = getItemsRet[2];
            branchInfo.push_back(branchEntry);
        }
    }
    return branchInfo;
}

RecipeSolver::RecipeSolver() {}

RecipeSolver::~RecipeSolver() {}

bool RecipeSolver::init(ItemDatabase &itemDb, RecipeDatabase &recipeDatabase, std::vector<std::string> recipeTypesToUse, PreferredItemDatabase &preferredItemDatabase)
{
    p_itemDb = &itemDb;
    p_recipeDatabase = &recipeDatabase;
    p_recipeTypesToUse = recipeTypesToUse;
    p_preferredItemDatabase = &preferredItemDatabase;
    updateCraftableRecipeCache();
    return true;
}

std::pair<bool, CraftingSolution> RecipeSolver::solve(const std::string &itemId, unsigned int count, const std::string &nbt, const std::vector<ItemEntry> &itemsInStorage, unsigned int timeout)
{
    std::vector<BranchInfo> solvedBranches;
    std::list<BranchInfo> aliveBranches = createInitialBranches(itemId, count, nbt, itemsInStorage);
    if (aliveBranches.size() == 0)
    {
        CraftingSolution craftingSolution;
        craftingSolution.message = "no recipe for " + itemId;
        if (nbt != "")
        {
            craftingSolution.message = craftingSolution.message + " with nbt " + nbt;
        }
        return std::pair<bool, CraftingSolution>(false, craftingSolution);
    }
    std::vector<size_t> branchToDelete;
    std::list<BranchInfo>::iterator it = aliveBranches.begin();
    for (size_t i = 0; i < aliveBranches.size(); i++)
    {
        std::advance(it, 1);
        if (branchIsSolved(*it))
        {
            branchToDelete.push_back(i);
            solvedBranches.push_back(*it);
        }
    }
    for (size_t i = 0; i < branchToDelete.size(); i++)
    {
        solvedBranches.erase(solvedBranches.begin() + branchToDelete[i]);
    }

    std::chrono::steady_clock::time_point lastPrint = std::chrono::steady_clock::now();
    int iterations = 0;
    BranchInfo lastBranch;
    std::cout << "starting solver loop\r\n";
    std::chrono::steady_clock::time_point startSolverTime = std::chrono::steady_clock::now();
    while (aliveBranches.size() > 0)
    {
        //timeout check
        if (timeout != 0 && startSolverTime + std::chrono::duration<unsigned int, std::milli>(timeout) < std::chrono::steady_clock::now())
        {
            break;
        }
        iterations += 1;
        BranchInfo branchInfo = *aliveBranches.begin();
        aliveBranches.pop_front();
        lastBranch = branchInfo;
        //check if branch is solved
        if (branchInfo.itemsToCraft.size() == 0)
        {
            solvedBranches.push_back(branchInfo);
            //todo: look for more than 1 solution
            break;
        }
        //print debug info
        if (std::chrono::steady_clock::now() - lastPrint > std::chrono::duration<unsigned int, std::milli>(1000))
        {
            int secsPassed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startSolverTime).count() / 1000;
            int avg = iterations / secsPassed;
            avg = floor(avg);
            std::cout << "branches " + std::to_string(aliveBranches.size()) + " cur length " + std::to_string(branchInfo.craftingChain.size()) + " itr " + std::to_string(iterations) + " avg/s " + std::to_string(avg) + "\r\n";
            lastPrint = std::chrono::steady_clock::now();
        }
        //grab item that needs crafting from branch
        if (branchInfo.craftingChain.size() >= maxBranchDepth)
        {
            continue;
        }
        ItemEntry curItem = branchInfo.itemsToCraft[0];
        branchInfo.itemsToCraft.erase(branchInfo.itemsToCraft.begin());
        //grab recipes for item
        std::vector<Recipe *> recipes = p_recipeDatabase->getRecipesFor(curItem.name, curItem.nbt, p_recipeTypesToUse, itemTypeFilters);
        if (recipes.size() == 0)
        {
            continue;
        }
        //start calculating recipe variations for item
        std::vector<BranchInfo> newBranches;
        for (size_t i = 0; i < recipes.size(); i++)
        {
            bool used = false;
            for (size_t j = 0; j < branchInfo.recipesUsed.size(); j++)
            {
                if (branchInfo.recipesUsed[j] == recipes[i])
                {
                    used = true;
                    break;
                }
            }
            if (!used)
            {
                //get all variants of recipe
                std::vector<Recipe> variants = getRecipeVariants(recipes[i], *p_itemDb, *p_preferredItemDatabase);
                for (size_t j = 0; j < variants.size(); j++)
                {
                    BranchInfo newBranchEntry = BranchInfo(branchInfo);
                    int craftsNeeded = ceil(curItem.count / variants[j].count);
                    std::vector<ItemEntry> itemCosts = getRecipeCost(&variants[j], craftsNeeded);
                    std::array<std::vector<ItemEntry>, 3> getItemsRet = getItems(itemCosts, newBranchEntry.itemsInStorage);
                    newBranchEntry.itemsInStorage = getItemsRet[0];
                    newBranchEntry.itemsToRequest = mergeItemEntryVectors(newBranchEntry.itemsToRequest, getItemsRet[1]);
                    newBranchEntry.itemsToCraft = mergeItemEntryVectors(newBranchEntry.itemsToRequest, getItemsRet[2]);
                    newBranchEntry.recipesUsed.push_back(recipes[i]);
                    std::vector<std::string>
                    requires;
                    for (size_t k = 0; k < getItemsRet[2].size(); k++)
                    {
                        requires.push_back(getItemsRet[2][k].name);
                    }
                    newBranchEntry.craftingChain.push_back(CraftingChainEntry(variants[j], requires, curItem.count, curItem.name));
                    newBranches.push_back(newBranchEntry);
                }
            }
        }
        for (size_t i = 0; i < newBranches.size(); i++)
        {
            if (newBranches[i].craftingChain.size() < maxBranchDepth)
            {
                std::list<BranchInfo>::iterator it = aliveBranches.begin();
                bool exists = false;
                for(size_t j = 0; j < aliveBranches.size(); j++)
                {
                    if(itemsToCraftVectorsAreEqual(newBranches[i].itemsToCraft, (*it).itemsToCraft))
                    {
                        exists = true;
                        break;
                    }
                    std::advance(it,1);
                }
                if(!exists)
                {
                    aliveBranches.push_back(newBranches[i]);
                }
            }
        }
    }

    std::chrono::time_point stopSolverTime = std::chrono::steady_clock::now();
    unsigned int timeTaken = std::chrono::duration_cast<std::chrono::milliseconds>(stopSolverTime - startSolverTime).count();
    std::cout << "took " + std::to_string(iterations) + " iterations and " + std::to_string(timeTaken) + " ms\r\n";

    BranchInfo bestSolution = getBestCraftingSolution(solvedBranches);
    CraftingSolution craftingSolution;
    if (bestSolution.craftingChain.size() != 0)
    {
        //todo: return crafting solution
        return std::pair<bool, CraftingSolution>(true, craftingSolution);
    }
    //todo: return failed message
    return std::pair<bool, CraftingSolution>(false, craftingSolution);
}

std::vector<ItemEntry> RecipeSolver::dumpCraftableItems()
{
    return p_craftableItemCache;
}