#include <iostream>
#include "Modules/ItemDatabase/ItemDatabase.h"
#include "Modules/RecipeDatabase/RecipeDatabase.h"
#include "Modules/RecipeSolver/RecipeSolver.h"
#include "Modules/SimpleFileIo/SimpleFileIo.h"

RecipeDatabase recipeDatabase = RecipeDatabase();
ItemDatabase itemDb = ItemDatabase();
PreferredItemDatabase preferredItemDatabase = PreferredItemDatabase();
RecipeSolver recipeSolver = RecipeSolver();

int main()
{
    if(!recipeDatabase.init("1.16.5", "dw20Modified"))
    {
        std::cout << "error initializing recipe db\r\n";
        throw "error initializing recipe db\r\n";
    }
    if(!itemDb.init("1.16.5"))
    {
        std::cout << "error initializing itemDb\r\n";
        throw "error initializing itemDb\r\n";
    }
    std::string jsonStr = SimpleFileIo::readFile("../DataFiles/1.16.5/itemDatabase/preferredItems.json");
    if(!preferredItemDatabase.init(jsonStr))
    {
        std::cout << "error initializing preferreditemDb\r\n";
        throw "error initializing preferredItemDb\r\n";
    }

    std::vector<std::string> recipeTypeFilters = 
    {
        "minecraft:crafting",
        "minecraft:smelting"
    };

    recipeSolver.init(itemDb, recipeDatabase, recipeTypeFilters, preferredItemDatabase);


    std::vector<ItemEntry> itemsInStorage;
    itemsInStorage.push_back(ItemEntry("minecraft:cobblestone", "", 256));
    itemsInStorage.push_back(ItemEntry("minecraft:redstone", "", 128));
    itemsInStorage.push_back(ItemEntry("minecraft:sand", "", 128));
    itemsInStorage.push_back(ItemEntry("minecraft:iron_ingot", "", 128));
    itemsInStorage.push_back(ItemEntry("minecraft:oak_log", "", 128));
    itemsInStorage.push_back(ItemEntry("minecraft:gold_ingot", "", 128));
    itemsInStorage.push_back(ItemEntry("create:copper_ingot", "", 128));



    recipeSolver.solve("ironchest:silver_to_gold_chest_upgrade", 1, "", itemsInStorage);
    std::cout << "program done\r\n";

}